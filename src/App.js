import React, { useState, useEffect } from "react";
import { PageLayout } from "./components/PageLayout";
import { AuthenticatedTemplate, UnauthenticatedTemplate, useMsal } from "@azure/msal-react";
import { loginRequest } from "./authConfig";
import Button from "react-bootstrap/Button";
import { callMsGraph } from "./graph";

function App() {

  const { instance, accounts } = useMsal();
  const [accessToken, setAccessToken] = useState(null);
  const name = accounts[0] && accounts[0].name;
  const [graphData, setGraphData] = useState(null);
  const [group, setGroup] = useState(null);
  const axios = require('axios').default;
  useEffect(() => {
    console.log('graphData---', graphData)
    console.log('group---', group)
    console.log('upn--',instance.getActiveAccount())
  }, [graphData, group]);

  const getGroupID = async (id, token) => {
    let ids = [];
    const headersContent =
    {
      headers: {
        'Authorization': `Bearer ${token}`,
        // 'Content-Type': 'application/json'
      }
    }
    const headers2 = {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    const body2 = {
      "ids": [group],
      "types": ["user"]
    }
    const body = {
      securityEnabledOnly: false
    } 

    await axios.post('https://graph.microsoft.com/v1.0/users/' + id + '/getMemberGroups',
      body,
      headersContent
    )
      .then(res => ids = res.data.value[2])
      .catch(err => console.log(err))

    await axios.post('https://graph.microsoft.com/v1.0/directoryObjects/getByIds',
      {
        "ids": [id],
        "types": ["user"]
      },
      headers2
    )
      .then(res => console.log(res))
      .catch(err => console.log(err))

    await axios.post('https://graph.microsoft.com/v1.0/me/checkMemberGroups',
      {
        "groupIds": [ids],
        "types": ["user"]
      },
      headers2
    )
      .then(res => console.log(res))
      .catch(err => console.log(err))

      await axios.get('https://graph.microsoft.com/v1.0/groups/'+ids,
      headers2
    )
      .then(res => console.log(res))
      .catch(err => console.log(err))

  }
  const RequestAccessToken = async () => {
    const request = {
      ...loginRequest,
      account: accounts[0]
    };
    console.log(accounts[0])
    let token = '';
    // Silently acquires an access token which is then attached to a request for Microsoft Graph data
    await instance.acquireTokenSilent(request).then((response) => {
      setAccessToken(response.accessToken);
      console.log(response.accessToken)
      token = response.accessToken;
      // callMsGraph(response.accessToken).then(response => {
      //   setGraphData(response);
      //   getGroupID(response.id, token);
      // });
    }).catch((e) => {
      instance.acquireTokenPopup(request).then((response) => {
        callMsGraph(response.accessToken).then(response => setGraphData(response));
      });
    });
  }
  return (
    <PageLayout>
      <AuthenticatedTemplate style={{ margin: 'auto' }}>
        <div style={{ alignContent: 'center', margin: 'auto' }}>
          <h5 className="card-title">Welcome {name}</h5>
          {accessToken ?
            <p>Access Token Acquired!</p>

            :
            <Button variant="secondary" onClick={RequestAccessToken}>Request Access Token</Button>
          }
        </div>
      </AuthenticatedTemplate>
      <UnauthenticatedTemplate>
        <p>You are not signed in! Please sign in.</p>
      </UnauthenticatedTemplate>
    </PageLayout>
  );
}

export default App;